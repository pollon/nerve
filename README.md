# Nerve JS 2


## Nerve example
```
	<!-- Create a button with a nerve -->
	
	<div data-nerve="sidepanel_color" ...>
	  I'm a button
	</div>
	
	<!-- Create an element with a muscle to react to the button's nerve -->
	
	<aside data-sidepanel_color="class"	data-class=".. Bgc(red)">
	  I'm an element that can be targeted by a nerve
	</aside>
	
	<!-- Use `..` to append the existing value of the attribute -->
```

## Setup

Just include the script in your head tag.

**Example**

	<head>
		<script src=<path-to-js-files>/nerve.js></script>
	</head>

